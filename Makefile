include .env
include .env.local
export

# COLORS
GREEN  := $(shell tput -Txterm setaf 2)
BLUE   := $(shell tput -Txterm setaf 4)
RESET  := $(shell tput -Txterm sgr0)

help:
	@echo ''
	@echo 'Utilisation :'
	@echo '  ${BLUE}make${RESET} ${GREEN}<command>${RESET}'
	@echo ''
	@echo 'Commandes :'
	@awk '/^[a-zA-Z\-_0-9]+:/ { \
		helpMessage = match(lastLine, /^## (.*)/); \
		if (helpMessage) { \
			helpCommand = substr($$1, 0, index($$1, ":")-1); \
			helpMessage = substr(lastLine, RSTART + 3, RLENGTH); \
			printf "  ${BLUE}%-$(TARGET_MAX_CHAR_NUM)s${RESET} ${GREEN}%s${RESET}\n", helpCommand, helpMessage; \
		} \
	} \
	{ lastLine = $$0 }' $(MAKEFILE_LIST)

## Préparation de l'environnement et démarrage des containers
start:
	@docker compose up

## Préparation de l'environnement et démarrage des containers et rend la main
start-d:
	@docker compose up -d

## Fermeture des containers
stop:
	@docker compose down

reset:
	@docker compose down
	@rm -rf ./var

## Fermeture et redémarrage des containers
restart: stop start

## Nettoyage complet des données liées à docker
prune:
	@docker compose down
	@docker system prune -a

## Ouverture d'un bash dans le container php
php:
	@docker compose exec -w "/var/www" php sh

## Ouverture d'un bash dans le container db
db:
	@docker compose exec db bash

## Ouverture d'un bash dans le container web
web:
	@docker compose exec web sh

## Ouverture d'un bash dans le container web
phpstan:
	@docker compose exec -w "/var/www" php "vendor/bin/phpstan"
