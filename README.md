# Done

## Owner
- Create an owner
- List owners

## Account
- Create an account

# Todo

## Owner
- Edit an owner
- Delete an owner

## Account
- Edit an account
- Delete an account
- List all accounts

## Category
- Create a category (with parent)
- Edit a category
- Delete a category
- List all categories

## Transaction
- Create a transaction
- Edit a transaction
- Delete a transaction
- List all transactions
- Import transactions from CSV

## Dashboard
- Total amount of money at a given date (today default)
- Total amount of money per account at a given date (today default)
- Total amount of money per category at a given date (today default)
- Total amount of money per month, between two dates
- Total amount of money per month per account, between two dates
- Total amount of money per month per category, between two dates
- Total amount of money per month per category per account, between two dates
- Total amount of money per day, between two dates
- Total amount of money per day per account, between two dates
- Total amount of money per day per category, between two dates
- Total amount of money per day per category per account, between two dates