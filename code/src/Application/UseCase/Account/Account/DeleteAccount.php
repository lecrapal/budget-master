<?php
declare(strict_types=1);

namespace App\Application\UseCase\Account\Account;

use App\Application\ViewModel\Shared\ViewModelInterface;
use App\Domain\Account\Repository\AccountRepositoryInterface;
use App\Domain\Account\ValueObject\AccountId;
use App\Domain\Shared\Exception\RepositoryException;
use App\Domain\Shared\Model\Error;

class DeleteAccount
{
    public function __construct(
        private readonly AccountRepositoryInterface $repository)
    {
    }

    public function execute(AccountId $account_id, ViewModelInterface $viewModel): void
    {
        try{
            $this->repository->delete($account_id);
        }catch (RepositoryException $e){
            $viewModel->setError(Error::create('account.delete.failed')->setException($e));
        }
    }
}