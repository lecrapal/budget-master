<?php
declare(strict_types=1);

namespace App\Application\UseCase\Account\Account;

use App\Application\ViewModel\Account\AccountsViewModelInterface;
use App\Domain\Account\Repository\AccountRepositoryInterface;
use App\Domain\Shared\Exception\RepositoryException;
use App\Domain\Shared\Model\Error;

class ListAccounts
{

    public function __construct(
        private readonly AccountRepositoryInterface $repository)
    {
    }

    public function execute(AccountsViewModelInterface $viewModel): void
    {
        try{
            $accounts = $this->repository->fetchAll();
            $viewModel->setAccounts($accounts);
        }catch (RepositoryException $e){
            $viewModel->setError(Error::create('account.list.failed')->setException($e));
        }
    }
}