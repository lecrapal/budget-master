<?php
declare(strict_types=1);

namespace App\Application\UseCase\Account\Account;

use App\Application\ViewModel\Account\AccountViewModelInterface;
use App\Domain\Account\Entity\Account;
use App\Domain\Account\Repository\AccountRepositoryInterface;
use App\Domain\Shared\Exception\RepositoryException;
use App\Domain\Shared\Model\Error;

class SaveAccount
{

    public function __construct(
        private readonly AccountRepositoryInterface $repository)
    {
    }

    public function execute(Account $account, AccountViewModelInterface $viewModel): void
    {
        try{
            $this->repository->save($account);
            $viewModel->setAccount($account);
        }catch (RepositoryException $e){
            $viewModel->setError(Error::create('account.save.failed')->setException($e));
        }
    }
}