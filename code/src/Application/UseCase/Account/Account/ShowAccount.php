<?php
declare(strict_types=1);

namespace App\Application\UseCase\Account\Account;

use App\Application\ViewModel\Account\AccountViewModelInterface;
use App\Domain\Account\Repository\AccountRepositoryInterface;
use App\Domain\Account\ValueObject\AccountId;
use App\Domain\Shared\Exception\RepositoryException;
use App\Domain\Shared\Model\Error;

class ShowAccount
{

    public function __construct(
        private readonly AccountRepositoryInterface $repository)
    {
    }

    public function execute(AccountId $accountId, AccountViewModelInterface $viewModel): void
    {
        try{
            $account = $this->repository->fetch($accountId);
            if($account === null){
                $viewModel->setError(Error::create('account.show.not_found'));
                return;
            }
            $viewModel->setAccount($account);
        }catch (RepositoryException $e){
            $viewModel->setError(Error::create('account.show.failed')->setException($e));
        }
    }
}