<?php
declare(strict_types=1);

namespace App\Application\UseCase\Account\Owner;

use App\Application\ViewModel\Shared\ViewModelInterface;
use App\Domain\Account\Repository\OwnerRepositoryInterface;
use App\Domain\Account\ValueObject\OwnerId;
use App\Domain\Shared\Exception\RepositoryException;
use App\Domain\Shared\Model\Error;

class DeleteOwner
{
    public function __construct(
        private readonly OwnerRepositoryInterface $repository)
    {
    }

    public function execute(OwnerId $owner_id, ViewModelInterface $viewModel): void
    {
        try{
            $this->repository->delete($owner_id);
        }catch (RepositoryException $e){
            $viewModel->setError(Error::create('owner.delete.failed')->setException($e));
        }
    }
}