<?php
declare(strict_types=1);

namespace App\Application\UseCase\Account\Owner;

use App\Application\ViewModel\Account\OwnersViewModelInterface;
use App\Domain\Account\Repository\OwnerRepositoryInterface;
use App\Domain\Shared\Exception\RepositoryException;
use App\Domain\Shared\Model\Error;

class ListOwners
{

    public function __construct(
        private readonly OwnerRepositoryInterface $repository)
    {
    }

    public function execute(OwnersViewModelInterface $viewModel): void
    {
        try{
            $owners = $this->repository->fetchAll();
            $viewModel->setOwners($owners);
        }catch (RepositoryException $e){
            $viewModel->setError(Error::create('account.list.failed')->setException($e));
        }
    }
}