<?php
declare(strict_types=1);

namespace App\Application\UseCase\Account\Owner;

use App\Application\ViewModel\Account\OwnerViewModelInterface;
use App\Domain\Account\Entity\Owner;
use App\Domain\Account\Repository\OwnerRepositoryInterface;
use App\Domain\Shared\Exception\RepositoryException;
use App\Domain\Shared\Model\Error;

class SaveOwner
{


    public function __construct(
        private readonly OwnerRepositoryInterface $repository)
    {
    }

    public function execute(Owner $owner, OwnerViewModelInterface $viewModel): void
    {
        try{
            $this->repository->save($owner);
            $viewModel->setOwner($owner);
        }catch (RepositoryException $e){
            $viewModel->setError(Error::create('owner.save.failed')->setException($e));
        }
    }
}