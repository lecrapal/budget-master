<?php
declare(strict_types=1);

namespace App\Application\UseCase\Account\Owner;

use App\Application\ViewModel\Account\OwnerViewModelInterface;
use App\Domain\Account\Repository\OwnerRepositoryInterface;
use App\Domain\Account\ValueObject\OwnerId;
use App\Domain\Shared\Exception\RepositoryException;
use App\Domain\Shared\Model\Error;

class ShowOwner
{

    public function __construct(
        private readonly OwnerRepositoryInterface $repository)
    {
    }

    public function execute(OwnerId $ownerId, OwnerViewModelInterface $viewModel): void
    {
        try{
            $owner = $this->repository->fetch($ownerId);
            if($owner === null){
                $viewModel->setError(Error::create('owner.show.not_found'));
                return;
            }
            $viewModel->setOwner($owner);
        }catch (RepositoryException $e){
            $viewModel->setError(Error::create('owner.show.failed')->setException($e));
        }
    }
}