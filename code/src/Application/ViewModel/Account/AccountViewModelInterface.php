<?php

declare(strict_types=1);

namespace App\Application\ViewModel\Account;

use App\Application\ViewModel\Shared\ViewModelInterface;
use App\Domain\Account\Entity\Account;

interface AccountViewModelInterface extends ViewModelInterface
{
    public function setAccount(Account $account): self;

    /**
     * @return Account|null
     */
    public function getAccount(): ?Account;
}