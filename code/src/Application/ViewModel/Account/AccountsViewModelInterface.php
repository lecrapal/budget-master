<?php
declare(strict_types=1);

namespace App\Application\ViewModel\Account;

use App\Application\ViewModel\Shared\ViewModelInterface;
use App\Domain\Account\Collection\AccountCollectionInterface;

interface AccountsViewModelInterface extends ViewModelInterface
{
    public function setAccounts(AccountCollectionInterface $accounts): self;

    public function getAccounts(): AccountCollectionInterface;
}