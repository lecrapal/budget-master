<?php

declare(strict_types=1);

namespace App\Application\ViewModel\Account;

use App\Application\ViewModel\Shared\ViewModelInterface;
use App\Domain\Account\Entity\Owner;

interface OwnerViewModelInterface extends ViewModelInterface
{
    public function setOwner(Owner $owner): self;

    /**
     * @return Owner|null
     */
    public function getOwner(): ?Owner;
}