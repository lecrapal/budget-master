<?php

declare(strict_types=1);

namespace App\Application\ViewModel\Account;

use App\Application\ViewModel\Shared\ViewModelInterface;
use App\Domain\Account\Collection\OwnerCollectionInterface;

interface OwnersViewModelInterface extends ViewModelInterface
{
    public function setOwners(OwnerCollectionInterface $owners): self;

    /**
     * @return OwnerCollectionInterface
     */
    public function getOwners(): OwnerCollectionInterface;
}