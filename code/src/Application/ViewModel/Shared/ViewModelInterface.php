<?php
declare(strict_types=1);

namespace App\Application\ViewModel\Shared;

use App\Domain\Shared\Model\Error;

interface ViewModelInterface
{
    public function setError(Error $error): self;
    public function getError(): ?Error;
}