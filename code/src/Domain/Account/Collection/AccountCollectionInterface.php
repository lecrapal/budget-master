<?php

declare(strict_types=1);

namespace App\Domain\Account\Collection;

use App\Domain\Account\Entity\Account;

interface AccountCollectionInterface
{
    public function addAccount(Account $account): void;

    public function removeAccount(Account $account): void;

    public function clear(): void;

    /**
     * @return Account[]
     */
    public function all(): array;
}