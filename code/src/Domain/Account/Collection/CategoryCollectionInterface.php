<?php

declare(strict_types=1);

namespace App\Domain\Account\Collection;

use App\Domain\Account\Entity\Category;

interface CategoryCollectionInterface
{
    public function addCategory(Category $category): void;

    public function removeCategory(Category $category): void;

    public function clear(): void;

    /**
     * @return Category[]
     */
    public function all(): array;
}