<?php

declare(strict_types=1);

namespace App\Domain\Account\Collection;

use App\Domain\Account\Entity\Owner;

interface OwnerCollectionInterface
{
    public function addOwner(Owner $owner): void;

    public function removeOwner(Owner $owner): void;

    public function clear(): void;

    /**
     * @return Owner[]
     */
    public function all(): array;
}