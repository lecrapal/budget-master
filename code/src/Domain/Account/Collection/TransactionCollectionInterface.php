<?php

declare(strict_types=1);

namespace App\Domain\Account\Collection;

use App\Domain\Account\Entity\Transaction;

interface TransactionCollectionInterface
{
    public function addTransaction(Transaction $transaction): void;

    public function removeTransaction(Transaction $transaction): void;

    public function clear(): void;

    /**
     * @return Transaction[]
     */
    public function all(): array;
}