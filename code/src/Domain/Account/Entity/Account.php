<?php

declare(strict_types=1);

namespace App\Domain\Account\Entity;

use App\Domain\Account\Collection\TransactionCollectionInterface;
use App\Domain\Account\ValueObject\AccountId;
use App\Domain\Shared\ValueObject\Money;
use BadMethodCallException;

class Account
{
    protected ?TransactionCollectionInterface $transactions = null;

    /**
     * @param AccountId $id
     * @param Money $balance
     * @param Owner $owner
     */
    public function __construct(
        protected AccountId $id,
        protected Money     $balance,
        protected Owner     $owner)
    {
    }

    /**
     * @return AccountId
     */
    public function getId(): AccountId
    {
        return $this->id;
    }

    /**
     * @param AccountId $id
     * @return Account
     */
    public function setId(AccountId $id): Account
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return Money
     */
    public function getBalance(): Money
    {
        return $this->balance;
    }

    /**
     * @param Money $balance
     * @return Account
     */
    public function setBalance(Money $balance): Account
    {
        $this->balance = $balance;
        return $this;
    }

    /**
     * @return Owner
     */
    public function getOwner(): Owner
    {
        return $this->owner;
    }

    /**
     * @param Owner $owner
     * @return Account
     */
    public function setOwner(Owner $owner): Account
    {
        $this->owner = $owner;
        return $this;
    }

    /**
     * @return TransactionCollectionInterface|null
     */
    public function getTransactions(): ?TransactionCollectionInterface
    {
        return $this->transactions;
    }

    /**
     * @param TransactionCollectionInterface|null $transactions
     * @return Account
     */
    public function setTransactions(?TransactionCollectionInterface $transactions): Account
    {
        $this->transactions = $transactions;
        return $this;
    }

    public function addTransaction(Transaction $transaction): void
    {
        if ($this->transactions === null) {
            throw new BadMethodCallException('Transactions collection is not set');
        }
        $this->transactions->addTransaction($transaction);
    }

    public function removeTransaction(Transaction $transaction): void
    {
        if ($this->transactions === null) {
            throw new BadMethodCallException('Transactions collection is not set');
        }
        $this->transactions->removeTransaction($transaction);
    }

    public function __toString(): string
    {
        return $this->id->getValue(). ' - '.$this->owner.' - '.$this->balance;
    }

}