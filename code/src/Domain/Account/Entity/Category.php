<?php

declare(strict_types=1);

namespace App\Domain\Account\Entity;

use App\Domain\Account\Collection\CategoryCollectionInterface;
use App\Domain\Account\ValueObject\CategoryId;
use BadFunctionCallException;

class Category
{

    protected ?CategoryCollectionInterface $children = null;

    public function __construct(
        protected CategoryId $id,
        protected string $name,
        protected ?Category $parent = null,
    ) {
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Category
     */
    public function setName(string $name): Category
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return Category|null
     */
    public function getParent(): ?Category
    {
        return $this->parent;
    }

    /**
     * @param Category|null $parent
     * @return Category
     */
    public function setParent(?Category $parent): Category
    {
        $this->parent = $parent;
        return $this;
    }

    /**
     * @return CategoryCollectionInterface|null
     */
    public function getChildren(): ?CategoryCollectionInterface
    {
        return $this->children;
    }

    /**
     * @param CategoryCollectionInterface|null $children
     * @return Category
     */
    public function setChildren(?CategoryCollectionInterface $children): Category
    {
        $this->children = $children;
        return $this;
    }

    /**
     * @param Category $category
     * @return void
     */
    public function addChild(Category $category): void
    {
        if ($this->children === null) {
            throw new BadFunctionCallException('Children collection is not set');
        }

        $this->children->addCategory($category);
    }

    /**
     * @param Category $category
     * @return void
     */
    public function removeChild(Category $category): void
    {
        if ($this->children === null) {
            throw new BadFunctionCallException('Children collection is not set');
        }

        $this->children->removeCategory($category);
    }

    public function __toString(): string
    {
        return $this->parent?->__toString().' - '.$this->name;
    }
}