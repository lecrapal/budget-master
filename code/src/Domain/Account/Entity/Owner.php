<?php

declare(strict_types=1);

namespace App\Domain\Account\Entity;

use App\Domain\Account\ValueObject\OwnerId;

class Owner
{
    public function __construct(
        protected OwnerId $id,
        protected string $first_name,
        protected string $last_name
    ) {
    }

    /**
     * @return OwnerId
     */
    public function getId(): OwnerId
    {
        return $this->id;
    }

    /**
     * @param OwnerId $id
     * @return $this
     */
    public function setId(OwnerId $id): Owner
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->first_name;
    }

    /**
     * @param string $first_name
     * @return Owner
     */
    public function setFirstName(string $first_name): Owner
    {
        $this->first_name = $first_name;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->last_name;
    }

    /**
     * @param string $last_name
     * @return Owner
     */
    public function setLastName(string $last_name): Owner
    {
        $this->last_name = $last_name;
        return $this;
    }

    public function getFullName(): string
    {
        return $this->first_name.' '.$this->last_name;
    }

    public function __toString(): string
    {
        return $this->getFullName();
    }
}