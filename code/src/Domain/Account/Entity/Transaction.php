<?php

declare(strict_types=1);

namespace App\Domain\Account\Entity;

use App\Domain\Account\ValueObject\TransactionId;
use App\Domain\Shared\ValueObject\Money;
use DateTimeImmutable;

class Transaction
{
    protected ?Category $category = null;
    protected string $label = '';
    protected string $description = '';
    protected ?string $comment = null;

    public function __construct(
        protected readonly TransactionId $id,
        protected DateTimeImmutable      $date,
        protected Money                  $amount,
        protected Account                $account
    )
    {
    }

    /**
     * @return TransactionId
     */
    public function getId(): TransactionId
    {
        return $this->id;
    }

    /**
     * @return Account
     */
    public function getAccount(): Account
    {
        return $this->account;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getDate(): DateTimeImmutable
    {
        return $this->date;
    }

    /**
     * @param DateTimeImmutable $date
     * @return Transaction
     */
    public function setDate(DateTimeImmutable $date): Transaction
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return Money
     */
    public function getAmount(): Money
    {
        return $this->amount;
    }

    /**
     * @param Money $amount
     * @return Transaction
     */
    public function setAmount(Money $amount): Transaction
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return Category|null
     */
    public function getCategory(): ?Category
    {
        return $this->category;
    }

    /**
     * @param Category|null $category
     * @return Transaction
     */
    public function setCategory(?Category $category): Transaction
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * @param string $label
     * @return Transaction
     */
    public function setLabel(string $label): Transaction
    {
        $this->label = $label;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Transaction
     */
    public function setDescription(string $description): Transaction
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @param string|null $comment
     * @return Transaction
     */
    public function setComment(?string $comment): Transaction
    {
        $this->comment = $comment;
        return $this;
    }
}