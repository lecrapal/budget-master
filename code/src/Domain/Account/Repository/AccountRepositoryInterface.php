<?php
declare(strict_types=1);

namespace App\Domain\Account\Repository;

use App\Domain\Account\Collection\AccountCollectionInterface;
use App\Domain\Account\Entity\Account;
use App\Domain\Account\ValueObject\AccountId;
use App\Domain\Shared\Exception\RepositoryException;

interface AccountRepositoryInterface
{
    /**
     * @param AccountId $id
     * @return Account|null
     * @throws RepositoryException
     */
    public function fetch(AccountId $id): ?Account;


    /**
     * @param Account $account
     * @return void
     * @throws RepositoryException
     */
    public function save(Account $account): void;

    /**
     * @param AccountId $account_id
     * @return void
     * @throws RepositoryException
     */
    public function delete(AccountId $account_id): void;

    /**
     * @return AccountCollectionInterface
     * @throws RepositoryException
     */
    public function fetchAll(): AccountCollectionInterface;
}