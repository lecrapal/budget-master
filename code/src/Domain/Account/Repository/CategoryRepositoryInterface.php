<?php
declare(strict_types=1);

namespace App\Domain\Account\Repository;

use App\Domain\Account\Collection\CategoryCollectionInterface;
use App\Domain\Account\Entity\Category;
use App\Domain\Account\ValueObject\CategoryId;

interface CategoryRepositoryInterface
{
    public function fetch(CategoryId $id): ?Category;

    public function save(Category $account): void;

    public function delete(Category $account): void;

    public function fetchAll(): CategoryCollectionInterface;
}