<?php
declare(strict_types=1);

namespace App\Domain\Account\Repository;


use App\Domain\Account\Collection\OwnerCollectionInterface;
use App\Domain\Account\Entity\Owner;
use App\Domain\Account\ValueObject\OwnerId;
use App\Domain\Shared\Exception\RepositoryException;

interface OwnerRepositoryInterface
{
    /**
     * @param OwnerId $id
     * @return Owner|null
     * @throws RepositoryException
     */
    public function fetch(OwnerId $id): ?Owner;

    /**
     * @param Owner $owner
     * @return void
     * @throws RepositoryException
     */
    public function save(Owner $owner): void;

    /**
     * @param OwnerId $id
     * @return void
     * @throws RepositoryException
     */
    public function delete(OwnerId $id): void;

    /**
     * @return OwnerCollectionInterface
     * @throws RepositoryException
     */
    public function fetchAll(): OwnerCollectionInterface;
}