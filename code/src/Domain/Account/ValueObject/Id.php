<?php
declare(strict_types=1);

namespace App\Domain\Account\ValueObject;

use InvalidArgumentException;

class Id
{
    protected string $value;

    final public function __construct(string $value)
    {
        if (empty($value)) {
            throw new InvalidArgumentException(get_class($this) .': id cannot be empty');
        }

        $this->value = $value;
    }

    /**
     * @param string $value
     * @return static
     */
    public static function create(string $value): self
    {
        return new static($value);
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function equals(Id $other): bool
    {
        return get_class($this) === get_class($other) && $this->value === $other->getValue();
    }

    public function __toString(): string
    {
        return $this->value;
    }
}