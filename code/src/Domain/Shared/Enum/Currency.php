<?php

declare(strict_types=1);

namespace App\Domain\Shared\Enum;
enum Currency
{
    case EUR;

    public function getSymbol(): string
    {
        return match ($this) {
            self::EUR => '€',
        };
    }
}