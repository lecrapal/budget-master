<?php
declare(strict_types=1);

namespace App\Domain\Shared\Exception;

use Exception;
use Throwable;

class RepositoryException extends Exception
{
    public function __construct(string $message, Throwable $previous = null)
    {
        parent::__construct($message, 0, $previous);
    }
}