<?php
declare(strict_types=1);

namespace App\Domain\Shared\Model;

use Exception;

class Error
{
    protected ?Exception $exception = null;

    public function __construct(
        private readonly string $message,
    ) {
    }

    public static function create(string $message): self
    {
        return new self($message);
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function getException(): ?Exception
    {
        return $this->exception;
    }

    public function setException(Exception $exception): self
    {
        $this->exception = $exception;
        return $this;
    }
}