<?php

declare(strict_types=1);

namespace App\Domain\Shared\ValueObject;

use App\Domain\Shared\Enum\Currency;

readonly class Money
{

    public function __construct(
        protected float $amount,
        protected Currency $currency
    ) {
    }

    public static function create(float $amount): self
    {
        return new self($amount, Currency::EUR);
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @return Currency
     */
    public function getCurrency(): Currency
    {
        return $this->currency;
    }

    public function getFormattedAmount(): string
    {
        return number_format($this->amount, 2, ',', ' ');
    }

    public function __toString(): string
    {
        return $this->getFormattedAmount().$this->currency->getSymbol();
    }
}