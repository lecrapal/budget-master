<?php
declare(strict_types=1);

namespace App\Infrastructure\Collection;

use App\Domain\Account\Collection\AccountCollectionInterface;
use App\Domain\Account\Entity\Account;
use Ramsey\Collection\AbstractCollection;

/**
 * @extends AbstractCollection<Account>
 */
class AccountCollection extends AbstractCollection implements AccountCollectionInterface
{
    public function addAccount(Account $account): void
    {
        $this->add($account);
    }

    public function removeAccount(Account $account): void
    {
        $this->remove($account);
    }

    public function all(): array
    {
        return $this->toArray();
    }

    public function getType(): string
    {
        return Account::class;
    }
}