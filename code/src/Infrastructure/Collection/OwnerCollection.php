<?php

declare(strict_types=1);

namespace App\Infrastructure\Collection;

use App\Domain\Account\Collection\OwnerCollectionInterface;
use App\Domain\Account\Entity\Owner;
use Ramsey\Collection\AbstractCollection;

/**
 * @extends AbstractCollection<Owner>
 */
class OwnerCollection extends AbstractCollection implements OwnerCollectionInterface
{
    public function addOwner(Owner $owner): void
    {
        $this->add($owner);
    }

    public function removeOwner(Owner $owner): void
    {
        $this->remove($owner);
    }

    /**
     * @return Owner[]
     */
    public function all(): array
    {
        return $this->toArray();
    }

    public function getType(): string
    {
        return Owner::class;
    }
}