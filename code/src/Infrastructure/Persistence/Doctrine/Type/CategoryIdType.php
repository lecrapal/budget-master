<?php

declare(strict_types=1);

namespace App\Infrastructure\Persistence\Doctrine\Type;

use App\Domain\Account\ValueObject\CategoryId;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\GuidType;

class CategoryIdType extends GuidType
{

    public function getName(): string
    {
        return 'category_id';
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform): string
    {
        if (!$value instanceof CategoryId) {
            throw ConversionException::conversionFailedInvalidType(
                $value,
                $this->getName(),
                ['null', CategoryId::class]
            );
        }

        return $value->getValue();
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): ?CategoryId
    {
        if (!is_string($value)) {
            return null;
        }

        return new CategoryId($value);
    }
}