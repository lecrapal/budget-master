<?php

declare(strict_types=1);

namespace App\Infrastructure\Persistence\Doctrine\Type;

use App\Domain\Shared\ValueObject\Money;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\GuidType;

class MoneyType extends GuidType
{

    public function getName(): string
    {
        return 'money';
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform): float
    {
        if (!$value instanceof Money) {
            throw ConversionException::conversionFailedInvalidType(
                $value,
                $this->getName(),
                ['null', Money::class]
            );
        }

        return $value->getAmount();
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): ?Money
    {
        if (!is_float($value)) {
            return null;
        }

        return Money::create($value);
    }
}