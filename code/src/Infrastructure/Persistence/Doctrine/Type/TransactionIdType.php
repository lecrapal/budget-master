<?php

declare(strict_types=1);

namespace App\Infrastructure\Persistence\Doctrine\Type;

use App\Domain\Account\ValueObject\TransactionId;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\GuidType;

class TransactionIdType extends GuidType
{

    public function getName(): string
    {
        return 'transaction_id';
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform): string
    {
        if (!$value instanceof TransactionId) {
            throw ConversionException::conversionFailedInvalidType(
                $value,
                $this->getName(),
                ['null', TransactionId::class]
            );
        }

        return $value->getValue();
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): ?TransactionId
    {
        if (!is_string($value)) {
            return null;
        }

        return new TransactionId($value);
    }
}