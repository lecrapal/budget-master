<?php

declare(strict_types=1);

namespace App\Infrastructure\Persistence\Repository;

use App\Domain\Account\Collection\AccountCollectionInterface;
use App\Domain\Account\Entity\Account;
use App\Domain\Account\Repository\AccountRepositoryInterface;
use App\Domain\Account\ValueObject\AccountId;
use App\Infrastructure\Collection\AccountCollection;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Account>
 */
class AccountRepository extends ServiceEntityRepository implements AccountRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Account::class);
    }

    public function fetch(AccountId $id): ?Account
    {
        return null;
    }

    public function save(Account $account): void
    {
        // TODO: Implement save() method.
    }

    public function delete(AccountId $account_id): void
    {
        // TODO: Implement delete() method.
    }

    public function fetchAll(): AccountCollectionInterface
    {
        return new AccountCollection();
    }

}