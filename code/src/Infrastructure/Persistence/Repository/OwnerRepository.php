<?php

declare(strict_types=1);

namespace App\Infrastructure\Persistence\Repository;

use App\Domain\Account\Collection\OwnerCollectionInterface;
use App\Domain\Account\Entity\Owner;
use App\Domain\Account\Repository\OwnerRepositoryInterface;
use App\Domain\Account\ValueObject\OwnerId;
use App\Domain\Shared\Exception\RepositoryException;
use App\Infrastructure\Collection\OwnerCollection;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Exception;

/**
 * @extends ServiceEntityRepository<Owner>
 */
class OwnerRepository extends ServiceEntityRepository implements OwnerRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Owner::class);
    }

    public function fetch(OwnerId $id): ?Owner
    {
        try {
            return $this->find($id);
        } catch (Exception $e) {
            throw new RepositoryException('owner.fetch.failed', $e);
        }
    }

    public function save(Owner $owner): void
    {
        try {
            $this->_em->persist($owner);
            $this->_em->flush();
        } catch (Exception $e) {
            throw new RepositoryException('owner.save.failed', $e);
        }
    }

    public function delete(OwnerId $id): void
    {
        $owner = $this->fetch($id);
        if ($owner === null) {
            throw new RepositoryException('owner.not_found');
        }

        try {
            $this->_em->detach($owner);
        } catch (Exception $e) {
            throw new RepositoryException('owner.delete.failed', $e);
        }
    }

    public function fetchAll(): OwnerCollectionInterface
    {
        try {
            $owners = $this->findAll();
            return new OwnerCollection($owners);
        } catch (Exception $e) {
            throw new RepositoryException('owner.fetch_all.failed', $e);
        }
    }

}