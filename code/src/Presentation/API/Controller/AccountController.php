<?php
declare(strict_types=1);

namespace App\Presentation\API\Controller;

use App\Application\UseCase\Account\Account\SaveAccount;
use App\Domain\Account\Entity\Account;
use App\Domain\Account\Repository\AccountRepositoryInterface;
use App\Domain\Account\Repository\OwnerRepositoryInterface;
use App\Domain\Account\ValueObject\AccountId;
use App\Domain\Shared\Exception\RepositoryException;
use App\Presentation\API\Validator\Account\AccountValidator;
use App\Presentation\API\View\Account\AccountView;
use App\Presentation\API\ViewModel\Account\AccountViewModel;
use App\Presentation\API\ViewModel\Shared\ValidatorViewModel;
use Exception;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AccountController extends AbstractController
{
    #[Route('/account/create', name: 'create_account', methods: ['POST'])]
    public function save(
        Request                    $request,
        AccountRepositoryInterface $account_repository,
        OwnerRepositoryInterface   $owner_repository,
    ): Response
    {
        // Validation de la requête
        $validator_view_model = new ValidatorViewModel();
        $validator = new AccountValidator($request);
        $validator_view_model->setErrors($validator->validate());
        if ($validator_view_model->hasErrors()) {
            $view = new AccountView($validator_view_model);
            try {
                return new JsonResponse($view->render());
            } catch (Exception $e) {
                return new JsonResponse($e->getMessage());
            }
        }

        $data = $validator->getData();

        // Vérification de l'existence du titulaire du compte
        $owner = null;
        try {
            $owner = $owner_repository->fetch($data['owner_id']);
        } catch (RepositoryException) {
        }
        if (is_null($owner)) {
            $validator_view_model->addError('owner_id', 'owner.not_found');
            $view = new AccountView($validator_view_model);
            return $view->render();
        }

        $account = new Account(
            AccountId::create(Uuid::uuid4()->toString()),
            $data['balance'],
            $owner
        );

        $view_model = new AccountViewModel();
        $save_account = new SaveAccount($account_repository);
        $save_account->execute($account, $view_model);

        $view = new AccountView($view_model);

        return $view->render();
    }
}