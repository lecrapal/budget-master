<?php
declare(strict_types=1);

namespace App\Presentation\API\Controller;

use App\Application\UseCase\Account\Owner\ListOwners;
use App\Application\UseCase\Account\Owner\SaveOwner;
use App\Domain\Account\Entity\Owner;
use App\Domain\Account\Repository\OwnerRepositoryInterface;
use App\Domain\Account\ValueObject\OwnerId;
use App\Presentation\API\Validator\Account\OwnerValidator;
use App\Presentation\API\View\Account\OwnersView;
use App\Presentation\API\View\Account\OwnerView;
use App\Presentation\API\ViewModel\Account\OwnersViewModel;
use App\Presentation\API\ViewModel\Account\OwnerViewModel;
use App\Presentation\API\ViewModel\Shared\ValidatorViewModel;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OwnerController extends AbstractController
{
    #[Route('/owner/create', name: 'create_owner', methods: ['POST'])]
    public function save(
        Request                    $request,
        OwnerRepositoryInterface   $repository,
    ): Response
    {
        // Validation de la requête
        $validator_view_model = new ValidatorViewModel();
        $validator = new OwnerValidator($request);
        $validator_view_model->setErrors($validator->validate());
        if ($validator_view_model->hasErrors()) {
            $view = new OwnerView($validator_view_model);
            return $view->render();
        }

        $data = $validator->getData();
        $owner = new Owner(
            OwnerId::create(Uuid::uuid4()->toString()),
            $data['first_name'],
            $data['last_name']
        );

        $view_model = new OwnerViewModel();
        $save_owner = new SaveOwner($repository);
        $save_owner->execute($owner, $view_model);

        $view = new OwnerView($view_model);

        return $view->render();
    }

    #[Route('/owner', name: 'list_owner', methods: ['GET'])]
    public function list(
        OwnerRepositoryInterface   $repository,
    ): Response
    {
        $view_model = new OwnersViewModel();
        $save_owner = new ListOwners($repository);
        $save_owner->execute($view_model);

        $view = new OwnersView($view_model);

        return $view->render();
    }


}