<?php

declare(strict_types=1);

namespace App\Presentation\API\Validator\Account;

use App\Domain\Account\ValueObject\OwnerId;
use App\Domain\Shared\ValueObject\Money;
use App\Presentation\API\Validator\Constraint\OwnerExist;
use App\Presentation\API\Validator\Shared\Validator;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Collection as AssertCollection;

class AccountValidator extends Validator
{
    /**
     * @return array{balance:Money,owner_id:OwnerId}
     */
    public function getData(): array
    {
        return [
            'balance' => Money::create(floatval($this->request->request->get('balance',0))),
            'owner_id' => OwnerId::create(strval($this->request->request->get('owner_id','')))
        ];
    }


    /**
     * @return AssertCollection
     */
    protected function getConstraints(): AssertCollection
    {
        return new Assert\Collection([
            'balance' => [
                new Assert\NotBlank([
                    'message' => 'balance.not_blank'
                ]),
                new Assert\Type('float'),
            ],
            'owner_id' => [
                new Assert\Type('string'),
                new Assert\NotBlank([
                    'message' => 'owner_id.not_blank'
                ]),
                new OwnerExist()
            ]
        ]);
    }
}