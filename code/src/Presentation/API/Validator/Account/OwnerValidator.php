<?php

declare(strict_types=1);

namespace App\Presentation\API\Validator\Account;

use App\Presentation\API\Validator\Shared\Validator;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Collection as AssertCollection;

class OwnerValidator extends Validator
{
    /**
     * @return array{first_name:string,last_name:string}
     */
    public function getData(): array
    {
        return [
            'first_name' => strval($this->request->request->get('first_name', '')),
            'last_name' => strval($this->request->request->get('last_name', ''))
        ];
    }


    /**
     * @return AssertCollection
     */
    protected function getConstraints(): AssertCollection
    {
        return new Assert\Collection([
            'first_name' => [
                new Assert\NotBlank([
                    'message' => 'first_name.not_blank'
                ]),
                new Assert\Type('string'),
                new Assert\Length([
                    'min' => 2,
                    'minMessage' => 'first_name.min_length'
                ])
            ],
            'last_name' => [
                new Assert\NotBlank([
                    'message' => 'last_name.not_blank'
                ]),
                new Assert\Type('string'),
                new Assert\Length([
                    'min' => 2,
                    'minMessage' => 'last_name.min_length'
                ])
            ]
        ]);
    }
}