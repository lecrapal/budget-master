<?php
declare(strict_types=1);

namespace App\Presentation\API\Validator\Constraint;

use Symfony\Component\Validator\Constraint;

class OwnerExist extends Constraint
{
    public string $message = 'owner.not_exist';

    public function validatedBy(): string
    {
        return OwnerExistValidator::class;
    }

    public function getTargets(): string
    {
        return self::PROPERTY_CONSTRAINT;
    }
}