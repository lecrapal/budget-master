<?php
declare(strict_types=1);

namespace App\Presentation\API\Validator\Constraint;

use App\Domain\Account\ValueObject\OwnerId;
use App\Domain\Shared\Exception\RepositoryException;
use App\Infrastructure\Persistence\Repository\OwnerRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

class OwnerExistValidator extends ConstraintValidator
{
    public function __construct(private readonly OwnerRepository $repository)
    {
    }

    /**
     * @param mixed $value
     * @param Constraint $constraint
     * @return void
     */
    public function validate(mixed $value, Constraint $constraint): void
    {
        if (!$constraint instanceof OwnerExist) {
            throw new UnexpectedTypeException($constraint, OwnerExist::class);
        }

        if ($value === null || $value === '') {
            return;
        }

        if (!is_string($value)) {
            // throw this exception if your validator cannot handle the passed type so that it can be marked as invalid
            throw new UnexpectedValueException($value, 'string');
        }


        try {
            $owner = $this->repository->fetch(OwnerId::create($value));
            if ($owner === null) {
                $this->context->buildViolation($constraint->message)
                    ->addViolation();
            }
        } catch (RepositoryException) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }

}