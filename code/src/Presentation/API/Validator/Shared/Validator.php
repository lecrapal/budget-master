<?php
declare(strict_types=1);

namespace App\Presentation\API\Validator\Shared;

use Stringable;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints\Collection as AssertCollection;

abstract class Validator
{
    public function __construct(protected Request $request)
    {}

    /**
     * @return array<string, string|Stringable>
     */
    public function validate(): array
    {
        $violations = Validation::createValidator()->validate($this->request->request->all(), $this->getConstraints());

        $errors = [];

        foreach ($violations as $violation) {
            $errors[$violation->getPropertyPath()] = $violation->getMessage();
        }
        return $errors;
    }

    protected abstract function getConstraints(): AssertCollection;

    /**
     * @return array<string, mixed>
     */
    public abstract function getData(): array;
}