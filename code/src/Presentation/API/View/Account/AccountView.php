<?php
declare(strict_types=1);

namespace App\Presentation\API\View\Account;

use App\Application\ViewModel\Account\AccountViewModelInterface;
use App\Presentation\API\View\Shared\View;
use App\Application\ViewModel\Shared\ViewModelInterface;
use App\Presentation\API\ViewModel\Shared\ValidatorViewModel;
use UnexpectedValueException;

class AccountView extends View
{
    public function __construct(
        protected ViewModelInterface $view_model
    )
    {
        if (
            !$this->view_model instanceof AccountViewModelInterface &&
            !$this->view_model instanceof ValidatorViewModel
        ) {
            throw new UnexpectedValueException(
                'The view_model should be an instance of AccountViewModelInterface or ValidatorViewModel not ' .
                get_class($this->view_model)
            );
        }
        parent::__construct($this->view_model);
    }

    /**
     * @return array<string, mixed>
     */
    protected function getData(): array
    {
        if ($this->view_model instanceof ValidatorViewModel) {
            return [
                'success' => false,
                'message' => '',
                'account' => [],
                'errors' => $this->view_model->getErrors()
            ];
        }

        if ($this->view_model instanceof AccountViewModelInterface) {

            if($this->view_model->getError() !== null){
                return [
                    'success' => false,
                    'message' => $this->view_model->getError()->getMessage(),
                    'owner' => [],
                    'errors' => []
                ];
            }

            $account = [];
            if ($this->view_model->getAccount() !== null) {
                $account = [
                    'id' => $this->view_model->getAccount()->getId()->getValue(),
                    'balance' => $this->view_model->getAccount()->getBalance()->getFormattedAmount(),
                    'owner' => $this->view_model->getAccount()->getOwner()->getFullName(),
                ];
            }

            return [
                'success' => true,
                'message' => '',
                'account' => $account,
                'errors' => []
            ];
        }

        return [
            'success' => true,
            'message' => '',
            'account' => [],
            'errors' => []
        ];
    }
}