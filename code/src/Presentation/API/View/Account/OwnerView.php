<?php
declare(strict_types=1);

namespace App\Presentation\API\View\Account;

use App\Application\ViewModel\Account\OwnerViewModelInterface;
use App\Application\ViewModel\Shared\ViewModelInterface;
use App\Presentation\API\View\Shared\View;
use App\Presentation\API\ViewModel\Shared\ValidatorViewModel;
use UnexpectedValueException;

class OwnerView extends View
{
    public function __construct(
        protected ViewModelInterface $view_model
    )
    {
        if (
            !$this->view_model instanceof OwnerViewModelInterface &&
            !$this->view_model instanceof ValidatorViewModel
        ) {
            throw new UnexpectedValueException(
                'The view_model should be an instance of OwnerViewModelInterface or ValidatorViewModel not '.
                get_class($this->view_model)
            );
        }
        parent::__construct($this->view_model);
    }

    /**
     * @return array<string, mixed>
     */
    public function getData(): array
    {
        if ($this->view_model instanceof ValidatorViewModel) {
            return [
                'success' => false,
                'message' => '',
                'owner' => [],
                'errors' => $this->view_model->getErrors()
            ];
        }

        if ($this->view_model instanceof OwnerViewModelInterface) {
            if($this->view_model->getError() !== null){
                return [
                    'success' => false,
                    'message' => $this->view_model->getError()->getMessage(),
                    'owner' => [],
                    'errors' => []
                ];
            }

            $owner = [];
            if($this->view_model->getOwner() !== null){
                $owner = [
                    'id' => $this->view_model->getOwner()->getId()->getValue(),
                    'first_name' => $this->view_model->getOwner()->getFirstName(),
                    'last_name' => $this->view_model->getOwner()->getLastName(),
                ];
            }

            return [
                'success' => true,
                'message' => '',
                'owner' => $owner,
                'errors' => []
            ];
        }

        return [
            'success' => true,
            'message' => '',
            'owner' => [],
            'errors' => []
        ];
    }
}