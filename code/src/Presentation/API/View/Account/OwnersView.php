<?php
declare(strict_types=1);

namespace App\Presentation\API\View\Account;

use App\Application\ViewModel\Account\OwnersViewModelInterface;
use App\Application\ViewModel\Shared\ViewModelInterface;
use App\Presentation\API\View\Shared\View;
use App\Presentation\API\ViewModel\Shared\ValidatorViewModel;
use UnexpectedValueException;

class OwnersView extends View
{
    public function __construct(
        protected ViewModelInterface $view_model
    )
    {
        if (
            !$this->view_model instanceof OwnersViewModelInterface &&
            !$this->view_model instanceof ValidatorViewModel
        ) {
            throw new UnexpectedValueException(
                'The view_model should be an instance of OwnersViewModelInterface or ValidatorViewModel not '.
                get_class($this->view_model)
            );
        }
        parent::__construct($this->view_model);
    }

    /**
     * @return array<string, mixed>
     */
    public function getData(): array
    {
        if ($this->view_model instanceof ValidatorViewModel) {
            return [
                'success' => false,
                'message' => '',
                'owner' => [],
                'errors' => $this->view_model->getErrors()
            ];
        }

        if ($this->view_model instanceof OwnersViewModelInterface) {
            if($this->view_model->getError() !== null){
                return [
                    'success' => false,
                    'message' => $this->view_model->getError()->getMessage(),
                    'owner' => [],
                    'errors' => []
                ];
            }

            $owners = [];
            foreach($this->view_model->getOwners()->all() as $owner){
                $owners[] = [
                    'id' => $owner->getId()->getValue(),
                    'full_name' => $owner->getFullname(),
                ];
            }
            return [
                'success' => true,
                'message' => '',
                'owner' => $owners,
                'errors' => []
            ];
        }

        return [
            'success' => true,
            'message' => '',
            'owner' => [],
            'errors' => []
        ];
    }
}