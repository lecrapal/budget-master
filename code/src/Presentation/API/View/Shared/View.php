<?php
declare(strict_types=1);

namespace App\Presentation\API\View\Shared;

use App\Application\ViewModel\Shared\ViewModelInterface;
use App\Presentation\API\ViewModel\Shared\ValidatorViewModel;
use App\Presentation\API\ViewModel\Shared\ViewModel;
use Symfony\Component\HttpFoundation\JsonResponse;

class View
{
    public function __construct(
        protected ViewModelInterface $view_model)
    {
    }

    /**
     * @return array<string, mixed>
     */
    protected function getData(): array
    {
        if ($this->view_model instanceof ViewModel) {
            return $this->view_model->getError() !== null ?
                [
                    'success' => false,
                    'message' => $this->view_model->getError()->getMessage()
                ] :
                [
                    'success' => true,
                    'message' => ''
                ];
        }
        if($this->view_model instanceof ValidatorViewModel)
        {
            return $this->view_model->hasErrors() ?
                [
                    'success' => false,
                    'message' => 'validation.failed',
                    'errors' => $this->view_model->getErrors()
                ] :
                [
                    'success' => true,
                    'message' => '',
                    'errors' => []
                ];
        }
        return [
                'success' => false,
                'message' => 'general.failed',
            ];
    }

    /**
     * @return JsonResponse
     */
    public function render(): JsonResponse
    {
        return new JsonResponse($this->getData());
    }
}