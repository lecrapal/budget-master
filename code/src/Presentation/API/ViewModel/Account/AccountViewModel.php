<?php
declare(strict_types=1);

namespace App\Presentation\API\ViewModel\Account;

use App\Application\ViewModel\Account\AccountViewModelInterface;
use App\Presentation\API\ViewModel\Shared\ViewModel;
use App\Domain\Account\Entity\Account;

class AccountViewModel extends ViewModel implements AccountViewModelInterface
{
    protected ?Account $account = null;

    public function setAccount(Account $account): self
    {
        $this->account = $account;
        return $this;
    }

    /**
     * @return Account|null
     */
    public function getAccount(): ?Account
    {
        return $this->account;
    }
}