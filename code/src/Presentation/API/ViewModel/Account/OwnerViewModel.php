<?php
declare(strict_types=1);

namespace App\Presentation\API\ViewModel\Account;

use App\Application\ViewModel\Account\OwnerViewModelInterface;
use App\Domain\Account\Entity\Owner;
use App\Presentation\API\ViewModel\Shared\ViewModel;

class OwnerViewModel extends ViewModel implements OwnerViewModelInterface
{
    protected ?Owner $owner = null;

    public function setOwner(Owner $owner): self
    {
        $this->owner = $owner;
        return $this;
    }

    /**
     * @return Owner|null
     */
    public function getOwner(): ?Owner
    {
        return $this->owner;
    }
}