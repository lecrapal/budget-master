<?php
declare(strict_types=1);

namespace App\Presentation\API\ViewModel\Account;

use App\Application\ViewModel\Account\OwnersViewModelInterface;
use App\Domain\Account\Collection\OwnerCollectionInterface;
use App\Infrastructure\Collection\OwnerCollection;
use App\Presentation\API\ViewModel\Shared\ViewModel;

class OwnersViewModel extends ViewModel implements OwnersViewModelInterface
{
    protected OwnerCollectionInterface $owners;

    public function __construct()
    {
        $this->owners = new OwnerCollection();
    }

    public function setOwners(OwnerCollectionInterface $owners): self
    {
        $this->owners = $owners;
        return $this;
    }

    /**
     * @return OwnerCollectionInterface
     */
    public function getOwners(): OwnerCollectionInterface
    {
        return $this->owners;
    }
}