<?php
declare(strict_types=1);

namespace App\Presentation\API\ViewModel\Shared;

use App\Application\ViewModel\Shared\ViewModelInterface;
use App\Domain\Shared\Model\Error;
use Exception;
use Stringable;

class ValidatorViewModel implements ViewModelInterface
{
    /**
     * @var array<string, string|Stringable>
     */
    protected array $errors = [];

    /**
     * @param array<string, string|Stringable> $errors
     */
    public function setErrors(array $errors): self
    {
        $this->errors = $errors;
        return $this;
    }

    /**
     * @return array<string, string|Stringable>
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    public function hasErrors(): bool
    {
        return count($this->errors) > 0;
    }

    public function addError(string $field, string $message): self
    {
        $this->errors[$field] = $message;
        return $this;
    }


    /**
     * @param Error $error
     * @return ViewModelInterface
     * @throws Exception
     */
    public function setError(Error $error): ViewModelInterface
    {
        throw new Exception('Not implemented');
    }

    /**
     * @return Error|null
     * @throws Exception
     */
    public function getError(): ?Error
    {
        throw new Exception('Not implemented');
    }


}