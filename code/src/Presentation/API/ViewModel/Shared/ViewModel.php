<?php
declare(strict_types=1);

namespace App\Presentation\API\ViewModel\Shared;

use App\Application\ViewModel\Shared\ViewModelInterface;
use App\Domain\Shared\Model\Error;

class ViewModel implements ViewModelInterface
{
    protected ?Error $error = null;

    public function setError(Error $error): self
    {
        $this->error = $error;
        return $this;
    }

    public function getError(): ?Error
    {
        return $this->error;
    }
}